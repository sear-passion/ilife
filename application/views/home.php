<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>数字生活云 iLiFe</title>
		<link rel="stylesheet" href="/statics/iLife.css" media="screen" />
		<link rel="stylesheet" href="http://cdn.staticfile.org/twitter-bootstrap/3.0.3/css/bootstrap.min.css
" media="screen" />
		</head>
	<body >
		<div class="navbar navbar-inverse navbar-fixed-top bs-docs-nav " role="banner">
		  <div class="container">
		    <div class="navbar-header">
		     <a class="navbar-brand" href="/"><span class="nav-icon glyphicon glyphicon-cloud"></span> 数字生活云 —— <i>iLiFe</i> </a>
		    </div>
		  </div>
		</div>

		<div id="wrapper">
			<div class="item-list">
			<?php $i=0;?>
			<?php foreach ($subscribe_list as $key=> $item):?>
				<?php if (!isset($object_list[$item->oid])) {continue;}?>
				 <div class="item-wrapper <?if ($i==0) {echo "select";$cur_sub = $item;}?>">
			      	<div  class="item-one" title="<?php echo $object_list[$item->oid]->oname;?>" key="<?php echo $item->oid;?>">
			      		<div class="cover">
			      			<span class="list-icon glyphicon glyphicon-<?php echo $object_list[$item->oid]->rule;?>"></span>
			      			<a href="###" class="item-text"><?php echo $object_list[$item->oid]->oname;?></a>
			      		</div>
			      	</div>
			      	<span class="traingle"></span>
			    </div>
			    <?php $i++?>
			  <?php endforeach;?>
			  
			    <div class="item-wrapper " id="add-item" >
			      	<div  class="item-one" title="选择订阅">
			      		<div class="cover">
			      			<span class="list-icon glyphicon glyphicon-plus"></span>
			      		</div>
			      	</div>
			    </div>

			</div>
			<div class="main-panel">
			
			
			<?php foreach ($subscribe_list as $key=> $item):?>
				<?php if (!isset($object_list[$item->oid])) {continue;}?>
				
				<div class="panel-wrapper" id="panel<?php echo $key?>" key="<?php echo $key?>"  <?if ($cur_sub->oid==$key) {echo "style='display:block;'";}?>>
					<div class="show-panel" >
						<div id="weather_<?php echo $key?>" class="weather" key="<?php echo $key?>">
						</div>
					</div>
					<div class="msg-handle " oid="<?php echo $key?>">
						<p class="msg-handle-tip">消息箱 </span></p>
						<span class="arrow glyphicon glyphicon-chevron-down"></span>
					</div>
					<div class="msg-panel">
						<ul class="msg-list-wrapper">
							<?php foreach ($corritor_list[$key] as  $subitem):?>
							<li class="msg-list unread" mid="11111" oid="<?php echo $key?>">
								<?php echo $object_list[$key]->oname;?>
							</li>
							<li class="msg-list " >
								数据：	<?php echo $subitem->value;?>
								时间：	<?php echo $subitem->createtime;?>
							</li>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			  <?php endforeach;?>

			</div>
		</div>

		<!--rss dialog-->
		<div class="modal fade" id="rss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">选择订阅：</div>
		      <div class="modal-content">
		      	<ul style="padding:0 10px;">
		      	
		      	<?php foreach ($object_list as $key=> $item):?>
		      		<li class="item-one <?php if (isset($subscribe_list[$key])) {echo "selected";}?>" title="<?php echo $item->oname?>" key="<?php echo $key?>">
			      		<div class="cover">
			      			<span class="list-icon glyphicon glyphicon-<?php echo $item->rule?>"></span>
			      			<a href="###" class="item-text"><?php echo $item->oname?></a>
			      		</div>
			      		<span class="ok-icon glyphicon glyphicon-ok"></span>
		      		</li>
		      	<?php endforeach;?>
		      	
		      	</ul>
		      	<div style="clear:both;"></div>
		      </div>
		      <div class="modal-footer border-none">
		        <button type="button" class="btn btn-primary" id="rssSave">保存</button>
		      </div>
		    </div>
		  </div>
		</div>


		<!--alert dialog-->
		<div class="modal alert" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog alert">
		    <div class="modal-content">
		      <div class="modal-header border-none"></div>
		      <h4 class="modal-title modal-title-padding " >请求错误</h4>
		      <div class="modal-footer border-none">
		        <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
		      </div>
		    </div>
		  </div>
		</div>


		<script src="http://cdn.staticfile.org/jquery/1.7.2/jquery.min.js"></script>
		<script src="http://cdn.staticfile.org/twitter-bootstrap/3.0.3/js/bootstrap.min.js
"></script>
		<script src="http://cdn.staticfile.org/highcharts/3.0.7/highcharts.js"></script>
	

		<script>

		var weather_data = {};
		
		var daily_x = ['Africa', 'America', 'Asia', 'Europe', 'Oceania'];
		var daily_y =  [{
	                name: 'Year 1800',
	                data: [107, 31, 635, 203, 2]
	            }, {
	                name: 'Year 1900',
	                data: [133, 156, 947, 408, 6]
	            }, {
	                name: 'Year 2008',
	                data: [973, 914, 4054, 732, 34]
	            }];

			
			<?php foreach ($corritor_list as $key=> $item):?>
			var data_x_<?php echo $key?>=[
						<?php foreach ($item as $subitem):?>'<?php echo $subitem->createtime?>',<?php endforeach;?>
				                  			];
		 	var data_y_<?php echo $key?> = [         		 	
			{
	            name: '<?php echo $object_list[$key]->oname;?>',
	            marker: {
	                symbol: 'square'
	            },
	            data: [
					<?php foreach ($item as $subitem):?><?php echo $subitem->value?>,<?php endforeach;?>
		   	            ]
	        }, 
	       ];
			weather_data['oid_<?php echo $key?>'] = {
					'x' :data_x_<?php echo $key?>,
					'y' :data_y_<?php echo $key?>
				};
		 	<?php endforeach;?> 

		 </script>

		 <script type="text/javascript" src="/statics/iLife.js"></script>

	</body>
</html>