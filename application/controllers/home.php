<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    $this->load->model('subscribe','subscribe',true);

	    $rs['object_list'] = $this->subscribe->get_object_list();
	    
	    $rs['subscribe_list'] = $this->subscribe->get_subscribe_suid(9527);

	    $rs['corritor_list'] = $this->subscribe->get_corritor_suid(9527);
	    
	    
		$this->load->view('home',$rs);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */