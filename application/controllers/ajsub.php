<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajsub extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    $this->load->model('subscribe','subscribe',true);
	    $a_oids = $this->input->post('oids');
	    $subscribe_list = $this->subscribe->get_subscribe_suid(9527);
	    $data = array();
	    $del_sids = array();
	    foreach ($a_oids as $oid){
	        if (empty($oid))continue;
	        if (!isset($subscribe_list[$oid])){
	            $data[] =array('suid'=>9527,'oid'=>$oid);
	        }
	    }
	    foreach ($subscribe_list as $key => $item){
	        if (!in_array($key, $a_oids)){
	            $del_sids[] =$item->sid;
	        }
	    }
	    if (!empty($data)){
	        $this->subscribe->set_subscribes($data);
	    }
	    if (!empty($del_sids)){
	        $this->subscribe->del($del_sids);
	    }
	    
	    echo json_encode(array('code'=>100000,'data'=>array()));
	    
	    die();
	}
	
	public function newcount()
	{
	    $this->load->model('subscribe','subscribe',true);
	    $data = $this->subscribe->get_corritor_newcount(9527);
	    echo json_encode(array('code'=>100000,'data'=>$data)); 
	    die();
	}
	
	public function newlastcid()
	{
	    $lastcid = $this->input->get('lastcid');
	    $oid = $this->input->get('oid');
	    $this->load->model('subscribe','subscribe',true);
	    $rs = $this->subscribe->get_subscribe_suid(9527);
	    if (isset($rs[$oid])){
	        $data = $rs[$oid];
	        $data->lastcid = $lastcid;
	        $this->subscribe->set_subscribe($data);
	        echo json_encode(array('code'=>100000,'data'=>array()));
	    }else{
	        echo json_encode(array('code'=>100001,'meaage'=>'err'));
	    }
	    die();
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */