<?php
/**
 *  [The Introduce of this file]   
 *
 * @author       chuansong,ZendZhi Team <chuansong@staff.sina.com.cn>
 * @copyright    copyright(2013) weibo.com all rights reserved
 * @since        2013-12-8
 * @version      0.1
 */
class subscribe extends CI_Model{
    
    private static $table_name = 'n_subscribe';
    
    /**
     * 新增/更新一条订阅关系
     * @param int $taobao_orderno
     * @param array $data
     */
    public function set_subscribe($data) {
        $bRet = $this->db->replace(self::$table_name,$data);
        if ($bRet === false) {
            return false;
        }
        return $this->db->insert_id();
    }
    
    public function set_subscribes(array $data) {
        $bRet = $this->db->insert_batch(self::$table_name,$data);
        if ($bRet === false) {
            return false;
        }
        return true;
    }   
    
    
    
    /**
     * 获取所有对象
     * @param unknown_type $suid
     * @return unknown
     */
    public function get_object_list() {
        $sql = "select * from n_object ";
        $query = $this->db->query($sql);
        $data = array();
        foreach ($query->result() as $item){
            $data[$item->oid] = $item;
        }
        return $data;
    }
    
    
    /**
     * 得到用户的订阅列表
     * @param unknown_type $suid
     * @return unknown
     */
    public function get_subscribe_suid($suid) {
        
		$sql = "select * from n_subscribe where suid=".$suid;
		$query = $this->db->query($sql);
        $data = array();
        foreach ($query->result() as $item){
            $data[$item->oid] = $item;
        }
        return $data;
    }
    
    /**
     * 订阅对象的数据列表
     * @param unknown_type $suid
     * @return unknown
     */
    public function get_corritor_suid($suid) {
    
        $sql = "select * from n_subscribe a inner join n_corritor b on a.oid=b.oid where a.suid=".$suid." order by b.oid,b.cid";
        $query = $this->db->query($sql);
        $data = array();
       	foreach ($query->result() as $item){
       	    if (!isset($data[$item->oid])){
       	        $data[$item->oid] = array();
       	    }
       	    $data[$item->oid][] = $item;
       	}

        return $data;
    }
    
    /**
     * 获取对象的最新列表>lastcid
     * @param unknown_type $suid
     * @return unknown
     */
    public function get_corritor_oid_lastcid($oid,$lastcid) {
    
        $sql = "select * from n_corritor a inner join n_object b on a.oid=b.oid  where a.oid=".$oid." and a.cid > ".$lastcid;
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }
    
    /**
     *
     * @param unknown_type $suid
     * @return unknown
     */
    public function get_corritor_newcount($suid) {
    
        $sql = "select count(*) as num,b.oid from n_corritor a inner join n_subscribe b on a.oid=b.oid  where b.suid=".$suid." and a.cid > b.lastcid group by a.oid";
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }
    
    
    public function del( $sids) {
        $sql = "delete  from ".self::$table_name." where sid in(".implode(',', $sids).")";
        $bRet = $this->db->_execute($sql);
        if ($bRet === false) {
            return false;
        }
        return true;
    }
   
}