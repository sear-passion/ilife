<?php

class n_object_model extends CI_Model{
	
    function __construct(){
    	$this->load->model('data/n_object','n_object',true);
        parent::__construct();
    }
    public function set($data) {
    	$oid = $data['oid'];
    	if(empty($oid)) {
    		//生成新的数据对象
    		return $this->n_object->set($data);
    	} 
    	//检查数据是否发生变化，变化则更新之
    	$object_data = $this->get($oid);
    	if($data['c_value'] == $object_data['c_value']) {
    		return true;
    	}
    	$this->load->model('n_corritor_model','n_corritor_model',true);
    	$corritor_data = array(
    	                    'oid' => $data['oid'],
    	                    'x'   => $data['c_x'],
    	                    'y'   => $data['c_y'],
    	                    'value' => $object_data['c_value']
    	);
    	if($this->n_corritor_model->set($corritor_data)) {
    		$data['trends'] = $data['c_value'] - $object_data['c_value'];
    		return $this->n_object->set($data);
    	}
    	return false;
    	
    	
    }
    
    public function get($oid) {
    	if(empty($oid)) {
    		return false;
    	}
    	return $this->n_object->get($oid);
    }
    
}

?>