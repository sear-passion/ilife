<?php

class n_corritor_model extends CI_Model{
	
    function __construct(){
    	$this->load->model('data/n_corritor','n_corritor',true);
        parent::__construct();
    }
    public function set($data) {
    	if(empty($data)) {
    		return false;
    	}
    	return $this->n_corritor->set($data);
    }
    
    public function getByOid($oid) {
    	if(empty($oid)) {
    		return false;
    	}
    	return $this->n_corritor->getByOid($oid);
    }
    
    public function getByCid($cid) {
    	if(empty($cid)) {
    		return false;
    	}
    	return $this->n_corritor->getByCid($cid);
    }
    
}

?>