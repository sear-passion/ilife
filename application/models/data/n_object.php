<?php
/**
 * [The Introduce of this file]   
 *
 * @author       chuansong,ZendZhi Team <chuansong@staff.sina.com.cn>
 * @copyright    copyright(2013) weibo.com all rights reserved
 * @since        2013-12-8
 * @version      0.1
 */
class n_object extends CI_Model {
	
	private static $table_name = 'n_object';
	
	public function set(array $data) {
		$bRet = $this->db->replace(self::$table_name,$data);
		if ($bRet === false) {
			return false;
		}
		return $this->db->insert_id ();
	}
	
	public function get($oid) {
		$sql = "select * from " . self::$table_name . " where oid={$oid}";
		$query =  $this->db->query ($sql);
		$rs = $query->result();
		
		if(empty($rs)) {
			return false;
		}
		$data = (array)$rs[0];
		return $data;

	}
}