<?php
/**
 * [The Introduce of this file]   
 *
 * @author       chuansong,ZendZhi Team <chuansong@staff.sina.com.cn>
 * @copyright    copyright(2013) weibo.com all rights reserved
 * @since        2013-12-8
 * @version      0.1
 */
class n_corritor extends CI_Model {
	
	private static $table_name = 'n_corritor';
	
	public function set(array $data) {
		$bRet = $this->db->insert(self::$table_name,$data);
		if ($bRet === false) {
			return false;
		}
		return $this->db->insert_id ();
	}
	
	public function getByOid($oid) {
		$sql = "select * from " . self::$table_name . " where oid={$oid} order by createtime";
		$query = $this->db->query ( $sql);
		return $query->result();
	}
	
	public function getByCid($cid) {
		$sql = "select * from " . self::$table_name . " where cid={$cid}";
		$query = $this->db->query ( $sql);
		$rs = $query->result();
		if(empty($rs)) {
			return false;
		}
		return $rs[0];
	}
}