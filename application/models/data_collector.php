<?php

class data_collector extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	public function processor_10($source) {
		$native_data = json_decode ( file_get_contents ( $source ), true );
		return $native_data ['weatherinfo'] ['temp'];
	
	}
	public function processor_3($source, $c_value) {
		if (empty ( $c_value ) || $c_value < 0) {
			$c_value = 1000;
		}
		$detal = rand ( 1, 10 );
		return ($c_value - $detal);
	}
	public function processor_4($source, $c_value) {
		if (empty ( $c_value ) || $c_value > 500) {
			$c_value = 5;
		}
		$detal = rand ( 1, 10 );
		return ($c_value + $detal);
	}
	public function processor_5($source, $c_value) {
		if (empty ( $c_value ) || $c_value < 0) {
			$c_value = 200;
		}
		$detal = rand ( 1, 10 );
		return ($c_value - $detal);
	}
	public function processor_2($source) {
		$native_data = file_get_contents ( $source );
		$pattern = '#<table id="xiang1">[^-]*<td>([\d]+)<img src#';
		preg_match ( $pattern, $native_data, $matches );
		return $matches [1];
	}
	public function data_collector() {
		include (APPPATH . 'config/data_source.php');
		foreach ( $data_source as $oid => $source ) {
			$this->load->model ( 'n_object_model', 'n_object_model', true );
			$object_data = $this->n_object_model->get ( $oid );
			if (empty ( $object_data )) {
				continue;
			}
			$c_value = call_user_func_array ( array ("data_collector", "processor_$oid" ), array ($source, $object_data ['c_value'] ) );
			$object_data ['c_value'] = $c_value;
			unset($object_data['c_time']);
			$this->n_object_model->set ( $object_data );
		}
	}

}

?>
